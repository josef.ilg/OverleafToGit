#!/bin/sh

if [ "$#" -ne 3 ]; then
        echo "Parameters are missing!"
        echo "      ./script.sh my_overleaf_name my_github_name my_github_repo_name" 
        exit 1
fi

git clone https://git.overleaf.com/$1
git clone https://gitlab.com/$2/$3.git

cp -r $1/* $3

cd $3
git add *
git commit -m "Transfer from Overleaf to this Repository"
git push origin master
cd ..
rm -r $1 $3

